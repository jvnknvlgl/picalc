# picalc.cpp

`picalc` is a tool to approximate Pi using series approximation. Originally written in C, it was rewritten in C++ to allow for easier multithreading.
