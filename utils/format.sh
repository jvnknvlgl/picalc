#!/bin/bash
find src -name '*.cpp' | xargs clang-format -i
find src -name '*.hpp' | xargs clang-format -i
