#include <array>
#include <cmath>
#include <cstdint>
#include <cstdlib>
#include <iomanip>
#include <iostream>
#include <thread>

#define THREAD_N 12

auto calc(const std::pair<uint32_t, uint32_t> &bnd, double &sum) {
    for (uint32_t i = bnd.first; i < bnd.second; i++)
        sum += std::pow(-1, i) / (2 * i + 1);
}

auto main(const int argc, char **argv) -> int {
    double pi = 0.0;

    if (argc < 2) {
        std::cout << "Usage: " << argv[0] << " <count>" << std::endl;
        return -1;
    }

    const uint32_t cnt = strtol(argv[1], nullptr, 0);

    std::cout << "Tool to find pi using series approximation" << std::endl;
    std::cout << "Copyright (c) 2018-2024 Jasper Vinkenvleugel\n" << std::endl;

    std::array<std::pair<uint32_t, uint32_t>, THREAD_N> bnds;

    for (uint32_t i = 0; i < bnds.size(); i++)
        bnds[i] = std::make_pair(i * cnt / THREAD_N, (i + 1) * cnt / THREAD_N);

    std::array<double, THREAD_N> sums = {0.0};
    std::array<std::thread, THREAD_N> thrs;

    for (uint32_t i = 0; i < thrs.size(); i++)
        thrs[i] = std::thread(calc, std::ref(bnds[i]), std::ref(sums[i]));

    for (uint32_t i = 0; i < thrs.size(); i++)
        if (thrs[i].joinable()) {
            thrs[i].join();

            pi += 4 * sums[i];
        }

    std::cout << "Approximation with " << cnt
              << " addition(s): " << std::setprecision(10) << pi << std::endl;

    return 0;
}
